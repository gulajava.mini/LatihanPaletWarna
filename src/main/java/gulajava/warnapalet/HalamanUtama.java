package gulajava.warnapalet;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.concurrent.Callable;

import bolts.Continuation;
import bolts.Task;

/**
 * Created by Gulajava Ministudio on 2/18/16.
 */
public class HalamanUtama extends AppCompatActivity {


    private Toolbar mToolbar;
    private ActionBar mActionBar;

    private ImageView mImageView;

    private TextView mTextView_Vibrant;
    private TextView mTextView_DarkVibrant;
    private TextView mTextView_LightVibrant;
    private TextView mTextView_Muted;
    private TextView mTextView_MutedDark;
    private TextView mTextView_MutedLight;

    private int ID_GAMBAR = R.drawable.sampelc1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.halamanutama);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        HalamanUtama.this.setSupportActionBar(mToolbar);

        mActionBar = HalamanUtama.this.getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setTitle(R.string.app_name);

        inisialiasiTampilan();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        HalamanUtama.this.getMenuInflater().inflate(R.menu.menu_utama, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemid = item.getItemId();
        switch (itemid) {

            case R.id.action_gambar1:

                ambilSwatch(R.drawable.sampelc1);
                return true;

            case R.id.action_gambar2:

                ambilSwatch(R.drawable.sampelc2);
                return true;

            case R.id.action_gambar3:

                ambilSwatch(R.drawable.sampelc3);
                return true;

            case R.id.action_gambar4:

                ambilSwatch(R.drawable.sampelc4);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void inisialiasiTampilan() {

        mImageView = (ImageView) findViewById(R.id.preview_gambar);
        mTextView_Vibrant = (TextView) findViewById(R.id.teks_vibrant);
        mTextView_DarkVibrant = (TextView) findViewById(R.id.teks_dark_vibrant);
        mTextView_LightVibrant = (TextView) findViewById(R.id.teks_light_vibrant);
        mTextView_Muted = (TextView) findViewById(R.id.teks_muted);
        mTextView_MutedDark = (TextView) findViewById(R.id.teks_muteddark);
        mTextView_MutedLight = (TextView) findViewById(R.id.teks_mutedlight);

        //jalankan ambil swatch
        ambilSwatch(R.drawable.sampelc1);
    }


    private void ambilSwatch(int kodegambar) {

        ID_GAMBAR = kodegambar;
        Picasso.with(HalamanUtama.this).load(ID_GAMBAR).into(mImageView);

        Task.callInBackground(new Callable<Bitmap>() {
            @Override
            public Bitmap call() throws Exception {

                return BitmapFactory.decodeResource(HalamanUtama.this.getResources(), ID_GAMBAR);
            }
        })
                .continueWith(new Continuation<Bitmap, Object>() {
                    @Override
                    public Object then(Task<Bitmap> task) throws Exception {

                        Bitmap bitmap = task.getResult();

                        if (bitmap != null && !bitmap.isRecycled()) {

                            Palette.from(bitmap).generate(mPaletteAsyncListener);
                        }

                        return null;
                    }
                }, Task.UI_THREAD_EXECUTOR);

    }


    Palette.PaletteAsyncListener mPaletteAsyncListener = new Palette.PaletteAsyncListener() {
        @Override
        public void onGenerated(Palette palette) {

            //warna coba diambil dari palet gambar
            setTampilanWarnaTeks(mTextView_Vibrant, palette.getVibrantSwatch());
            setTampilanWarnaTeks(mTextView_DarkVibrant, palette.getDarkVibrantSwatch());
            setTampilanWarnaTeks(mTextView_LightVibrant, palette.getLightVibrantSwatch());
            setTampilanWarnaTeks(mTextView_Muted, palette.getMutedSwatch());
            setTampilanWarnaTeks(mTextView_MutedDark, palette.getDarkMutedSwatch());
            setTampilanWarnaTeks(mTextView_MutedLight, palette.getLightMutedSwatch());

            setToolbarWarna(palette.getDarkVibrantSwatch());


        }
    };


    private void setTampilanWarnaTeks(TextView textView, Palette.Swatch swatch) {

        if (swatch != null) {

            int idWarnaTeks = swatch.getTitleTextColor();
            int idWarnaLatar = swatch.getRgb();

            textView.setTextColor(idWarnaTeks);
            textView.setBackgroundColor(idWarnaLatar);
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);
        }
    }


    private void setToolbarWarna(Palette.Swatch swatch) {

        if (swatch != null) {

            int idWarnaLatar = swatch.getRgb();
            mToolbar.setBackgroundColor(idWarnaLatar);

            try {
                if (Build.VERSION.SDK_INT >= 21) {
                    HalamanUtama.this.getWindow().setStatusBarColor(idWarnaLatar);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

}
